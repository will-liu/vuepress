<!--
 * @Author: Will
 * @Date: 2022-07-04 15:23:13
 * @LastEditors: Will
 * @LastEditTime: 2022-07-26 20:53:40
 * @Description: 请填写简介
-->
### k8s client-go
-   init config
-   create namespace
-   get namespace
-   delete namespace
-   create deployment
-   get pod
-   delete pod
-   create service
-   get service
-   delete service
-   create ingress
-   get ingress
-   delete ingress