<!--
 * @Author: Will
 * @Date: 2022-07-03 10:47:07
 * @LastEditors: Will
 * @LastEditTime: 2022-12-03 13:06:26
 * @Description: 请填写简介
-->
<!--
 * @Author: Will
 * @Date: 2022-07-02 22:33:41
 * @LastEditors: Will
 * @LastEditTime: 2022-07-02 23:12:09
 * @Description: 请填写简介
-->
# 创建数据库并指定字符集

### redis
### mysql
### mongo
-   createUser
``` sh
    db.createUser(
    {
        user: "deex",
        pwd: "deex123456",
        roles:[{role: "userAdmin" , db:"amaxChain"}]
    })
```
### linux
-   防火墙端口开放
>   sudo iptables -I INPUT -p tcp -m tcp --dport 16081 -j ACCEPT
-   保存防火墙修改
>   service iptables save
-   把专用密钥添加到 ssh-agent 的高速缓存中：
>   ssh-add  ~/.ssh/id_dsa
### kurbenetes
-   ingress rewrite-target
``` yaml
    apiVersion: networking.k8s.io/v1beta1
    kind: Ingress
    metadata:
    annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /$2
    name: rewrite
    namespace: default
    spec:
        rules:
        - host: rewrite.bar.com
            http:
            paths:
            - backend:
                serviceName: http-svc
                servicePort: 80
                path: /something(/|$)(.*)
```
>   rewrite.bar.com/something rewrites to rewrite.bar.com/
### centos install docker-compose
-   yum -y install python3-pip
-   pip3 install --upgrade pip
-   pip3 install docker-compose
### jenkins 安装
-   添加Yum源
>   sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo

-   导入密钥
>   sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key

-   安装
>   sudo yum install -y jenkins

-   重载服务（由于前面修改了Jenkins启动脚本）
>   sudo systemctl daemon-reload

-   启动Jenkins服务
>   sudo systemctl start jenkins

-   将Jenkins服务设置为开机启动
#由于Jenkins不是Native Service，所以需要用chkconfig命令而不是systemctl命令
>   sudo /sbin/chkconfig jenkins on