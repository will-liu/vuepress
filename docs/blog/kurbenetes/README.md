<!--
 * @Author: Will
 * @Date: 2022-07-29 11:07:02
 * @LastEditors: Will
 * @LastEditTime: 2022-11-07 11:06:31
 * @Description: 请填写简介
-->
# kurbenetes
### helm
-   install
    -   mac
        >   brew install helmfile
    -   linux
        >   pacman -S helmfile
    -   helm-diff
        >   helm plugin install https://github.com/databus23/helm-diff
### config
-   ingress rewrite-target
``` yaml
    apiVersion: networking.k8s.io/v1beta1
    kind: Ingress
    metadata:
    annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /$2
    name: rewrite
    namespace: default
    spec:
        rules:
        - host: rewrite.bar.com
            http:
            paths:
            - backend:
                serviceName: http-svc
                servicePort: 80
                path: /something(/|$)(.*)
```
>   rewrite.bar.com/something rewrites to rewrite.bar.com/
-   rewrite 重新二
``` yaml
    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
    name: ingress
    namespace: test
    annotations:
        kubernetes.io/ingress.class: "nginx"
        nginx.ingress.kubernetes.io/use-regex: "true"
        nginx.ingress.kubernetes.io/server-snippet: |
            if ($uri ~* "/v1/tfs/.*") {
                rewrite ^/v1/tfs/(.*)$ https://gogen-test.oss-cn-hangzhou.aliyuncs.com/$1 permanent;
            }
    spec:
    tls:
    - hosts:
        - nginx-a.gogen.cn
        secretName: gogen.cn
    rules:
    - host: nginx-a.gogen.cn
        http:
        paths:
        - path: /
            backend:
            serviceName: nginx-a
            servicePort: 80
        - path: /.*.(txt|css|doc)
            backend:
            serviceName: nginx-b
            servicePort: 80
        - path: /(api|app)/
            backend:
            serviceName: nginx-c
            servicePort: 80
        - path: /api
            backend:
            serviceName: nginx-d
            servicePort: 80
```
### tgbot config
-   register: https://my.telegram.org/auth
-   docker build [Dockerfile](./Dockerfile)
-   docker-compose file [docker-compose.yaml](./docker-compose.yaml)
### k8s config external service
-   config endpoints [endpoint.yaml](./endpoint-service.yaml)
-   config ingress [ingress.yaml](./ingress.yaml)
