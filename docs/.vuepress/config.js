/*
 * @Author: Will
 * @Date: 2022-07-04 14:46:46
 * @LastEditors: Will
 * @LastEditTime: 2022-08-02 15:02:20
 * @Description: 请填写简介
 */
module.exports = {
    title: '楚门的世界',
    description: '分享生活的喜悦, 工作上的共同进步',
    base: '/',
    dest: 'public',
    head: [
        ['link', { rel: 'icon', href: '/logo.png' }]
    ],
    themeConfig: {
        displayAllHeaders: true,
        activeHeaderLinks: false, 
        nav: [
            { 
            text: "首页", 
            link: "/" 
            },
            { 
            text: "工作", 
            items: [
                { text: '博客', link: '/blog/' },
                { text: '任务', link: '/task/' }
            ]
            },
            { 
            text: "跑步", 
            link: "/run/" 
            },
        ],
        sidebar: {
            '/blog/': [
            {
                title: '楚门的博客',
                collapsible: true,
                children: [
                    [ '/blog/blockchain/', 'blockchain' ],
                    [ '/blog/golang/', 'golang' ],
                    [ '/blog/kurbenetes/', 'kurbenetes' ],
                    [ '/blog/linux/', 'linux' ],
                    [ '/blog/mongo/', 'mongo' ],
                    [ '/blog/mysql/', 'mysql' ],
                    [ '/blog/redis/', 'redis' ],
                    [ '/blog/network/', 'network' ]
                ],
            },
            ],
            '/task/': [
            {
                title: '工作任务',
                collapsible: true,
                children: [
                    [ '/task/week/', '本周(7.4-7.10)'],
                    [ '/task/history/', '历史']
                ]
            }
            ],
            '/run/': [
            {
                title: '跑步计划',
                collapsible: true,
                children: [
                [ '/run/week/', '本周(7.4-7.10)'],
                [ '/run/history/', '历史']
                ]
            }
            ]
        },
        lastUpdated: '上次更新',
    },
}