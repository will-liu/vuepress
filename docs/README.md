---
home: true
heroImage: /logo.png
actionText: 进入博客 →
actionLink: /blog/
features:
- title: 专注于工作
  details: 享受工作
- title: 跑步
  details: 热爱跑步, 记录每一滴汗水
footer: MIT Licensed | Copyright 2022 Will Liu
---